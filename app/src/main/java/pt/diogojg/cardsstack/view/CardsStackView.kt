package pt.diogojg.cardsstack.view

import android.content.Context
import android.graphics.Point
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.util.AttributeSet
import android.view.View
import pt.diogojg.cardsstack.Card
import pt.diogojg.cardsstack.CardStack
import pt.diogojg.cardsstack.R
import pt.diogojg.cardsstack.multitouchlistener.MultiTouchListener


/**
 * @author Diogo Garcia
 */
class CardsStackView : ConstraintLayout, MultiTouchListener.Listener {
    private var cardStack = CardStack(emptyList())
    private var baseCardElevation = 0f

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        baseCardElevation = resources.getDimensionPixelSize(R.dimen.base_card_elevation).toFloat()
    }

    fun setCards(cards: List<Card>) {
        cardStack = CardStack(cards)
        removeAllViews()
        addCardsToLayout()

        topCardView().setOnTouchListener(MultiTouchListener(this))
    }

    fun getCardsStack() = cardStack

    private fun moveCardToBottom(anchorPoint: Point) {
        val topCardView = topCardView()
        val topCard = cardStack.topCard()
        cardStack.moveTopCardToBottomOfStack()

        topCardView.animate()
                .translationX(anchorPoint.x.toFloat())
                .translationY(anchorPoint.y.toFloat())
                .rotation(topCard.angle)
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(200L)
                .withEndAction {
                    topCardView.animate()
                            .translationX(0f)
                            .translationY(0f)
                            .setDuration(400L)
                            .start()
                    animateAllCardsElevation()
                    topCardView.setOnTouchListener(null)
                    topCardView().setOnTouchListener(MultiTouchListener(this))
                }
                .start()
    }

    private fun moveCardToBottom() {
        val topCardView = topCardView()
        cardStack.moveTopCardToBottomOfStack()

        topCardView.animate()
                .translationX(0f)
                .translationY(0f)
                .setDuration(400L)
                .start()
        animateAllCardsElevation()
        topCardView.setOnTouchListener(null)
        topCardView().setOnTouchListener(MultiTouchListener(this))
    }

    private fun animateAllCardsElevation() {
        var cardIndex = 0
        cardStack.stack().forEach {
            val cardView = findViewWithTag<PhotoCardView>(it)
            cardView.animate()
                    .translationZ(getElevationForCard(cardIndex))
                    .setDuration(200L)
                    .start()
            cardIndex++
        }
    }

    private fun topCardView(): PhotoCardView = findViewWithTag(cardStack.topCard())


    private fun getElevationForCard(cardIndex: Int): Float {
        return (cardStack.size() - cardIndex) * baseCardElevation
    }

    override fun onActionUp() {
        if (canTopCardGoDirectlyToBottomOfStack())
            moveCardToBottom()
        else
            moveCardToBottom(getBestAnchoringPoint())
    }

    private fun canTopCardGoDirectlyToBottomOfStack(): Boolean {
        val topCardView = topCardView()
        return (topCardView.translationY < topAnchorPoint().y ||
                topCardView.translationX > rightAnchorPoint().x ||
                topCardView.translationY > bottomAnchorPoint().y ||
                topCardView.translationX < leftAnchorPoint().x) &&
                topCardView.scaleX == 1f &&
                topCardView.scaleY == 1f &&
                topCardView.rotation == cardStack.topCard().angle
    }

    private fun getBestAnchoringPoint(): Point {
        val topCardView = topCardView()
        val topCardPosition = Point(topCardView.translationX.toInt(), topCardView.translationY.toInt())

        val topDistance = distanceBetween(topCardPosition, topAnchorPoint())
        val bottomDistance = distanceBetween(topCardPosition, bottomAnchorPoint())
        val leftDistance = distanceBetween(topCardPosition, leftAnchorPoint())
        val rightDistance = distanceBetween(topCardPosition, rightAnchorPoint())

        val anchorDistances = HashMap<Point, Float>()
        anchorDistances[topAnchorPoint()] = topDistance
        anchorDistances[bottomAnchorPoint()] = bottomDistance
        anchorDistances[leftAnchorPoint()] = leftDistance
        anchorDistances[rightAnchorPoint()] = rightDistance

        var targetAnchorPoint = topAnchorPoint()
        var shortestDistance = topDistance

        anchorDistances.keys.forEach {
            if (anchorDistances[it]!! < shortestDistance) {
                targetAnchorPoint = it
                shortestDistance = anchorDistances[it]!!
            }
        }
        return targetAnchorPoint
    }

    private fun addCardsToLayout() {
        var index = 0
        cardStack.stack().forEach {
            val cardView = createCardView(it, index)
            val lp = ConstraintLayout.LayoutParams(0, 0)
            addView(cardView, lp)
            applyLayoutConstraintsToCard(cardView)
            index++
        }
    }

    private fun createCardView(card: Card, index: Int): View {
        val cardView = PhotoCardView(context)
        cardView.setCard(card)
        cardView.rotation = card.angle
        cardView.id = View.generateViewId()
        cardView.translationZ = getElevationForCard(index)
        cardView.tag = card
        cardView.radius = resources.getDimension(R.dimen.card_radius)
        return cardView
    }

    private fun applyLayoutConstraintsToCard(targetView: View) {
        val constraintSet = ConstraintSet()
        constraintSet.clone(this)
        constraintSet.setDimensionRatio(targetView.id, "5:4")
        constraintSet.constrainPercentWidth(targetView.id, 0.5f)
        constraintSet.connect(targetView.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
        constraintSet.connect(targetView.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
        constraintSet.connect(targetView.id, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT)
        constraintSet.connect(targetView.id, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT)
        constraintSet.applyTo(this)
    }

    private fun distanceBetween(pointA: Point, pointB: Point): Float {
        return Math.sqrt(Math.pow(pointB.x - pointA.x.toDouble(), 2.0) + Math.pow(pointB.y - pointA.y.toDouble(), 2.0)).toFloat()
    }

    private fun topAnchorPoint(): Point {
        return Point(0, -(topCardView().height + topCardView().width * 0.2).toInt())
    }

    private fun bottomAnchorPoint(): Point {
        return Point(0, (topCardView().height + topCardView().width * 0.2).toInt())
    }

    private fun leftAnchorPoint(): Point {
        return Point(-(topCardView().width + topCardView().height * 0.2).toInt(), 0)
    }

    private fun rightAnchorPoint(): Point {
        return Point((topCardView().width + topCardView().height * 0.2).toInt(), 0)
    }
}
package pt.diogojg.cardsstack.view

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_number_card.view.*
import pt.diogojg.cardsstack.Card
import pt.diogojg.cardsstack.R


/**
 * @author Diogo Garcia
 */
class PhotoCardView : CardView {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_number_card, this)
    }

    fun setCard(card: Card) {
        Glide.with(context)
                .load(card.imageUrl)
                .into(view_number_card_image)
    }
}
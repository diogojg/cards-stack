package pt.diogojg.cardsstack

/**
 * @author Diogo Garcia
 */
data class Card(val imageUrl: String,
                val angle: Float)
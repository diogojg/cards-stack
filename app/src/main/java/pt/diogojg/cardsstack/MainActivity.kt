package pt.diogojg.cardsstack

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val KEY_STATE = "state"

    override fun onCreate(savedInstanceState: Bundle?) {
        Hawk.init(this).build()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val savedState = getSavedState()
        val cartStack = if (savedState == null) {
            val cardStackGenerator = CardStackGenerator()
            CardStack(cardStackGenerator.generateStack(10))
        } else
            savedState

        activity_main_stack.setCards(cartStack.cards())
    }

    override fun onDestroy() {
        saveStateToSharedPref()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        saveStateToSharedPref()
        super.onSaveInstanceState(outState)
    }

    private fun saveStateToSharedPref() {
        Hawk.put(KEY_STATE, activity_main_stack.getCardsStack())
    }

    private fun getSavedState(): CardStack? {
        return Hawk.get(KEY_STATE)
    }
}

package pt.diogojg.cardsstack

import java.util.*

/**
 * @author Diogo Garcia
 */
class CardStackGenerator {
    fun generateStack(sizeOfStack: Int): List<Card> {
        if (sizeOfStack < 1) {
            throw Exception("Size of stack must be greater than zero.")
        }

        val stack = ArrayList<Card>()

        for (i in 0 until sizeOfStack) {
            val card = Card(photoList[i % photoList.size], randomAngle())
            stack.add(card)
        }
        return stack
    }

    private fun randomAngle(): Float {
        return Random().nextFloat() * (10 + 10) - 10
    }

    private val photoList = listOf(
            "https://citizen-femme.com/wp-content/uploads/2017/11/praca-do-comercio-lisbon-GettyImages-648812458.jpg",
            "https://thetravelhack.com/wp-content/uploads/2018/02/Pena-National-Palace-day-trip-from-Lisbon.jpg",
            "https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/sa6549607c78f5c11/image/ic981eebf15c54bd8/version/1471521184/image.jpg",
            "https://cdn.pixabay.com/photo/2016/11/05/22/13/lisbon-1801727_960_720.jpg",
            "https://zap.aeiou.pt/wp-content/uploads/2015/05/14b206c46d5111c8c445c54785e1692c.jpg",
            "https://travel.usnews.com/static-travel/images/destinations/106/tram_28-3-getty.jpg",
            "https://eduportugal.eu/wp-content/uploads/2018/02/candidaturas-liciaturas-portugal-Eduardo-Vii-Park-In-Lisbon-Eduportugal.jpg",
            "https://cdn.images.express.co.uk/img/dynamic/134/590x/Torre-Belem-Lisbon-Portugal-954001.jpg",
            "http://www.gofordmagazine.pt/wp-content/uploads/2015/05/RC10_web.jpg",
            "https://www.maat.pt/sites/default/files/2017-09/017%202.jpg"
    )
}
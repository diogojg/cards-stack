package pt.diogojg.cardsstack

/**
 * @author Diogo Garcia
 */
class CardStack(cards: List<Card>) {
    private val cards: ArrayList<Card> = ArrayList(cards)

    fun moveTopCardToBottomOfStack() {
        if (cards.isEmpty()) return
        if (cards.size == 1) return

        val topCard = cards.first()
        cards.removeAt(0)
        cards.add(topCard)
    }

    fun topCard(): Card = cards.first()

    fun isEmpty() = cards.isEmpty()

    fun size() = cards.size

    fun stack() = cards.toList()

    fun cards() = cards
}